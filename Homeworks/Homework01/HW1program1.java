// десять чисел вводятся с клавиатуры
// вывести количество чисел, у которых сумма цифр меньше 18
 
import java.util.Scanner;

class HW1program1 {
	public static void main(String[] args) {
		System.out.println ("Введите 10 целых чисел");
		Scanner scanner = new Scanner(System.in);
		int amount = 0;
		

		for (int i=0; i<10; i++) {
			
			int sum = 0;
			int number = scanner.nextInt();
		
			if (number < 0) {
				number = number * -1;
			}

			while (number != 0) {
				sum = sum + number % 10;
				number = number / 10;	
			}

			if (sum<18) {
				amount ++;
			}	
		}
		
		System.out.println ("Количество чисел с суммой цифр меньше 18 - " + amount);

	}
}
