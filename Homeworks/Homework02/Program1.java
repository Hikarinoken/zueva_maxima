// необходимо вывести двоичное представление числа number
// гарантируется что number>=0, <=127
// не используем: массивы, циклы, строки, Integer.
// эх, циклы-то за что?)0

import java.util.Scanner;

class Program1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int number = scanner.nextInt();
		int a = number % 2;
		int b = (number/2) % 2;
		int c = (number/4) % 2;
		int d = (number/8) % 2;
		int e = (number/16) % 2;
		int f = (number/32) % 2;
		int g = (number/64) % 2;


		System.out.println(g + "" + f + "" + e + "" + d + "" + c + "" + b + "" + a);
	}
}
