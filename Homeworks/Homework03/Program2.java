// найти кол-во локальных минимумов в числе
import java.util.Scanner;

class Program2 {
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);	
		
		int number = scanner.nextInt();	
		
		int counter = 0;
		

		while (number != 0) {

			int firstDig = number % 10;
			int secondDig = (number/10) % 10;
			int thirdDig = (number/100) % 10;

			if (firstDig > secondDig && thirdDig > secondDig) {
				counter++;
				number = number / 10;
			} 
			else {
			
			}	
			number = number / 10;
		}

		System.out.println("Количество локальных минимумов в числе - " + counter);
	}
}
