// на вход подается послед-сть чисел. посчитать сумму чисел
// последнее число = -1
//найти и вывести число, у которых сумма цифр минимальная среди остальных

import java.util.Scanner;

class Program1 {
	public static void main(String[] args) {
		Scanner scanner  = new Scanner(System.in);
		
		int number = scanner.nextInt();
		
		int minsum = 81;
		//на самом деле это макс.сум скорее. так как максимальное число до переполнения которое мы сможем записать - 999999999
		//точнее максимальным числом будет 1111111111, но сумма цифр максимальна с девятками
		int sum = 0;
		int a = number;

		while (number != -1 && number >= 0) {
			a = number;
			int remainder = number % 10;
			sum = sum + remainder;
				if (sum < minsum)
					{
						minsum = sum;
					}
			number = scanner.nextInt();	
		}
		System.out.println("минимальная сумма цифр у числа:" + a);		 
	}
}

// ну и выводит она не совсем то. да, это число с минимальной суммой цифр, но ПОСЛЕДНЕЕ из введённых
// то есть если будет подряд 10 1 100, выведет 100

